const express = require('express')
const app = express()
const port = process.env.PORT || 3002
const date = require('./date')
const morgan = require('morgan')
const car = require('./routes/cars')


app.use(express.json())
app.use('/api/cars', car)
app.use(morgan('tiny'))

/** MIDDLEWARE. Es un bloque de código en express que se ejecuta entre la petición que hace el usuario (request (req)) hasta que recibe la respuesta (response (res))
 * Funciones que tienen acceso al objeto Request, al objeto Responce y a la siguiente función Middleware que se conoce como next.
 * Pueden ejecutar cualquier tipo de código, incluso cambios en los objetos request y response. Suele utilizarse para revisar si los usuarios estan logeados.
 * Si no se agrega el método next() no se termina el ciclo de la petición del usuario, ya que next() es necesario para pasar al siguiente middleware y cerrar el ciclo.
 */
app.use(date)

// Los siguientes Middlewares solo se ejecutarán cuando su endpoint agregado sea consultado
/*
app.use('/api/cars/list', function (req, res, next){
  console.log('Request Type:', req.method)
  next()
})

app.use('/api/login/', function (req, res, next){
  console.log('login')
  next()
})
*/

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(port, () => console.log("Escuchando en el puerto:", port))